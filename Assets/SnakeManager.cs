using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeManager : MonoBehaviour
{

    public GameObject snakeHead;
    public GameObject snakeBody;
    public GameObject snakeTail;

    //int sizeOfSnakeBody = 2;

    LinkedList<GameObject> snakeBodyParts;

    const float MoveSpeed = 1f;//0.25f;

    SnakeDirection snakeDirection;

    SnakeDirection tailDirectionOnLastUpdate;

    const float leftWall = -5f;
    const float rightWall = 5f;
    const float topWall = 5f;
    const float bottomWall = -5f;

    // Start is called before the first frame update
    void Start()
    {
        snakeBodyParts = new LinkedList<GameObject>();
        snakeBodyParts.AddLast(snakeBody);
        snakeDirection = SnakeDirection.Up;


        snakeBodyParts.AddLast(snakeTail);


        //snakeBody.SetActive(false);
        //ResizeSnakeBody();
    }

    // Update is called once per frame
    void Update()
    {

        #region Input Controls

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (snakeDirection != SnakeDirection.Down)
            {
                snakeDirection = SnakeDirection.Up;
            }
        }
        if (Input.GetKeyDown(KeyCode.A)
            && snakeDirection != SnakeDirection.Right)
        {
            snakeDirection = SnakeDirection.Left;
        }
        if (Input.GetKeyDown(KeyCode.S)
            && snakeDirection != SnakeDirection.Up)
        {
            snakeDirection = SnakeDirection.Down;
        }
        if (Input.GetKeyDown(KeyCode.D)
            && snakeDirection != SnakeDirection.Left)
        {
            snakeDirection = SnakeDirection.Right;
        }

        #endregion

        if (Input.GetKeyDown(KeyCode.P))
        {
            AddOneBodyPartToSnake();
        }

        // if (Input.GetKeyDown(KeyCode.S))
        // {
        //     sizeOfSnakeBody--;
        //     ResizeSnakeBody();
        // }

        #region Snake Movement

        if (snakeDirection == SnakeDirection.Left)
        {
            snakeHead.transform.position += new Vector3(-MoveSpeed * Time.deltaTime, 0, 0);
        }
        else if (snakeDirection == SnakeDirection.Right)
        {
            snakeHead.transform.position += new Vector3(MoveSpeed * Time.deltaTime, 0, 0);
        }
        else if (snakeDirection == SnakeDirection.Up)
        {
            snakeHead.transform.position += new Vector3(0, MoveSpeed * Time.deltaTime, 0);
        }
        else if (snakeDirection == SnakeDirection.Down)
        {
            snakeHead.transform.position += new Vector3(0, -MoveSpeed * Time.deltaTime, 0);
        }


        GameObject lastB = snakeHead;
        SnakeDirection lastDir = snakeDirection;

        foreach (GameObject b in snakeBodyParts)
        {
            Vector3 difInPos = lastB.transform.position - b.transform.position;

            //Debug.Log("sanity test");

            if (lastDir == SnakeDirection.Up || lastDir == SnakeDirection.Down)
            {
                if (difInPos.x < 0)
                {
                    Debug.Log("dif.x < 0");
                    float moveAmount = -MoveSpeed * Time.deltaTime;

                    if (moveAmount < difInPos.x)
                        moveAmount = difInPos.x;

                    b.transform.position += new Vector3(moveAmount, 0, 0);

                    lastDir = SnakeDirection.Left;
                }
                else if (difInPos.x > 0)
                {
                    Debug.Log("dif.x > 0");
                    float moveAmount = MoveSpeed * Time.deltaTime;

                    if (moveAmount > difInPos.x)
                        moveAmount = difInPos.x;

                    b.transform.position += new Vector3(moveAmount, 0, 0);

                    lastDir = SnakeDirection.Right;
                }
                else if (difInPos.y < 0)
                {
                    float moveAmount = -MoveSpeed * Time.deltaTime;

                    if (moveAmount < difInPos.y)
                        moveAmount = difInPos.y;

                    Debug.Log("dif.y < 0");
                    b.transform.position += new Vector3(0, moveAmount, 0);

                    lastDir = SnakeDirection.Down;
                }
                else if (difInPos.y > 0)
                {
                    float moveAmount = MoveSpeed * Time.deltaTime;

                    if (moveAmount > difInPos.y)
                        moveAmount = difInPos.y;

                    Debug.Log("dif.y > 0");
                    b.transform.position += new Vector3(0, moveAmount, 0);

                    lastDir = SnakeDirection.Up;
                }
            }
            else
            {
                if (difInPos.y < 0)
                {
                    float moveAmount = -MoveSpeed * Time.deltaTime;

                    if (moveAmount < difInPos.y)
                        moveAmount = difInPos.y;

                    Debug.Log("dif.y < 0");
                    b.transform.position += new Vector3(0, moveAmount, 0);

                    lastDir = SnakeDirection.Down;
                }
                else if (difInPos.y > 0)
                {
                    float moveAmount = MoveSpeed * Time.deltaTime;

                    if (moveAmount > difInPos.y)
                        moveAmount = difInPos.y;

                    Debug.Log("dif.y > 0");
                    b.transform.position += new Vector3(0, moveAmount, 0);

                    lastDir = SnakeDirection.Up;
                }
                else if (difInPos.x < 0)
                {
                    Debug.Log("dif.x < 0");
                    float moveAmount = -MoveSpeed * Time.deltaTime;

                    if (moveAmount < difInPos.x)
                        moveAmount = difInPos.x;

                    b.transform.position += new Vector3(moveAmount, 0, 0);

                    lastDir = SnakeDirection.Left;
                }
                else if (difInPos.x > 0)
                {
                    Debug.Log("dif.x > 0");
                    float moveAmount = MoveSpeed * Time.deltaTime;

                    if (moveAmount > difInPos.x)
                        moveAmount = difInPos.x;

                    b.transform.position += new Vector3(moveAmount, 0, 0);

                    lastDir = SnakeDirection.Right;
                }
            }

            if (b == snakeTail)
                tailDirectionOnLastUpdate = lastDir;

            lastB = b;
        }


        //snakeTail.transform.position += new Vector3(-MoveSpeed * Time.deltaTime, 0, 0);


        #endregion


        #region check for lose condtion

        if(snakeHead.transform.position.x < leftWall)
            Debug.Log("hit left wall");
            

        

        #endregion

    }

    private void AddOneBodyPartToSnake()
    {
        GameObject b = Instantiate(snakeBody);
        b.GetComponent<SpriteRenderer>().color = Color.grey;
        b.SetActive(true);

        //LinkListNode<GameObject> n = new LinkListNode<GameObject>();

        // snakeBodyParts.Last.Value

        snakeBodyParts.AddBefore(snakeBodyParts.Last, b);//.AddLast(b);

        b.transform.position = snakeTail.transform.position;


        //check which way the tail is moving

        if(tailDirectionOnLastUpdate == SnakeDirection.Up)
            snakeTail.transform.position = snakeTail.transform.position + new Vector3(0, -1, 0);
        else if(tailDirectionOnLastUpdate == SnakeDirection.Down)
            snakeTail.transform.position = snakeTail.transform.position + new Vector3(0, 1, 0);
        else if(tailDirectionOnLastUpdate == SnakeDirection.Right)
            snakeTail.transform.position = snakeTail.transform.position + new Vector3(-1, 0, 0);
        else if(tailDirectionOnLastUpdate == SnakeDirection.Left)
            snakeTail.transform.position = snakeTail.transform.position + new Vector3(1, 0, 0);
    
    
    }

    // private void ResizeSnakeBody()
    // {
    //     foreach (GameObject b in snakeBodyParts)
    //     {
    //         Destroy(b);
    //     }

    //     snakeBodyParts.Clear();

    //     GameObject lastB = snakeHead;

    //     for (int i = 0; i < sizeOfSnakeBody; i++)
    //     {
    //         GameObject b = Instantiate(snakeBody);
    //         b.GetComponent<SpriteRenderer>().color = Color.grey;
    //         b.SetActive(true);

    //         snakeBodyParts.AddLast(b);
    //         b.transform.position = lastB.transform.position + new Vector3(1, 0, 0);
    //         lastB = b;
    //     }

    //     snakeTail.transform.position = lastB.transform.position + new Vector3(1, 0, 0);
    // }
}

public enum SnakeDirection
{
    Up,
    Down,
    Left,
    Right
}
