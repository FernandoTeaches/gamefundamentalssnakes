using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{

    public GameObject circle1, circle2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //transform.position += new Vector3(-0.1f * Time.deltaTime, 0, 0);



        // circle1.transform.position
        // circle2.transform.position

        float radius = 0.5f;

        float xDif, yDif;
        float hyp;

        xDif = circle1.transform.position.x - circle2.transform.position.x;
        yDif = circle1.transform.position.y - circle2.transform.position.y;

        xDif = Mathf.Abs(xDif);
        yDif = Mathf.Abs(yDif);

        hyp = Mathf.Sqrt(xDif * xDif + yDif * yDif);

        Debug.Log("hyp == " + hyp);

        if(hyp <= radius *2f)
            Debug.Log("hit!");

    }


    // void OnMouseEnter()
    // {
    //     Debug.Log("enter");
    // }

    // // ...the red fades out to cyan as the mouse is held over...
    // void OnMouseOver()
    // {
    //     Debug.Log("over");
    // }

    // // ...and the mesh finally turns white when the mouse moves away.
    // void OnMouseExit()
    // {
    //     Debug.Log("exit");
    // }

    // void OnCollisionEnter2D(Collision2D col)
    // {
    //     Debug.Log("OnCollisionEnter2D");
    // }

    //void OnCollisionEnter2D(Collision2D collision) => Log(collision);
}
